'use strict';


angular.module('mainMod').service('MessageManager', ['$http', function ($http) {

    return {

        getMessages: function () { // json file in root dir
        	return $http.get('/data.json');
        }
    }

}]);