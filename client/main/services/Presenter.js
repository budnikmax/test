'use strict';


angular.module('mainMod').service('Presenter', ['$timeout', function ($timeout) {

    return {

        checkIfImage: function (message) {
            var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})([=]{1,2})?$");
        
            return base64Matcher.test(message.data) ? true : false;
        },

        checkToRemove: function (messages) {
           messages = _.reject(messages, function (message) {
                return message.date.isBefore(moment())
            }); // if expired - remove it from DOM
            return messages;
        },

        setExpirationDate: function(messages) {
            messages = _.map(messages, function (message) {
                var randomDigit = Math.floor(Math.random() * (10 - 1 + 1)) + 1;// random digit to countdown expiration date
                message.date = moment().add(randomDigit, 's');

                return message;
            });

            return messages;
        }
    }

}]);