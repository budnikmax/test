'use strict';


angular.module('mainMod').service('Consumer', ['$timeout', 'MessageManager', function ($timeout, MessageManager) {

    return {

        getMessages: function () { // mocked data: couple of images and regular text
        	return MessageManager.getMessages();
        }
    }

}]);