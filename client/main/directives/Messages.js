'use strict';

angular.module('mainMod').directive('messages', function(){
	return {
		restrict : 'E',
		scope : {},
		templateUrl : "/client/views/messages.html",
		controller: "mainCtrl"
	}
});