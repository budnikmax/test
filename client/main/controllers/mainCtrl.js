'use strict';

angular.module('mainMod').controller('mainCtrl', ['$scope', '$interval', 'Consumer', 'Presenter', function ($scope, $interval, Consumer, Presenter) {
	
	var cancel; 	// variable to stop interval function if we have no messages


	Consumer.getMessages()
		.success(function (response) {
			$scope.messages = Presenter.setExpirationDate(response.messages); // set expiration date for received messages
			
			cancel = $interval(function () { // check every second if any of the messages are expired
				if ($scope.messages.length > 0) {
					$scope.messages = Presenter.checkToRemove($scope.messages);
				} else {
					stopInterval();
				}
			}, 1000);
		})
		.error(function (err) {
			alert("sorry, can't load messages due to\n" + err)
		});

	var stopInterval = function() { // destroys interval function if message array is empty
		if (!_.isUndefined(cancel)) {
			$interval.cancel(cancel);
			cancel = undefined;
		}
	};

	$scope.checkIfImage = function (message) {
		return Presenter.checkIfImage(message);
	}
}]);