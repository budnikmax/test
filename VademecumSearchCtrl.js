'use strict';

 /*
  * Vademecum products list controller (with infinity scroll)
  */


angular.module('vademecumMod').controller('VademecumSearchCtrl', ['$scope', '$rootScope', 'VademecumManager', 'ActionManager', function($scope, $rootScope, VademecumManager, ActionManager) {

    //=====================================================================
    // Local Variables
    //=====================================================================

    $rootScope.showVademecumTour = false;
    $scope.data = {};
    
    $scope.pagination = {};
    $scope.pagination.itemsPerPage = 10;
    $scope.pagination.offset = 0;
    $scope.pagination.limit = $scope.pagination.itemsPerPage;

    //=====================================================================
    // Scope functions and AJAX queries
    //=====================================================================

    ActionManager.getAllSpecies().then(function (response) {
        $scope.data.cropFamilies = response.data;
    }, function () {
        showErrorToast("Lo sentimos, no se han podido cargar las especies vegetales.");
    });

    $scope.nextPage = function (plague) {
        if (plague) {
            $scope.loading = true;
            $scope.pagination.onContentLoading = true; //prevent loading while waiting for precious ajax call

            VademecumManager.getProductsFitoForPlague($scope.pagination.offset, $scope.pagination.limit, plague).then(function (response) {
                $scope.data.productsFito = $scope.data.productsFito.concat(response.plain()); // array of products

                if (response.plain().length < $scope.pagination.itemsPerPage) {
                    $scope.pagination.endOfPage = true; //stop loading when all elements were loaded
                }
                $scope.pagination.onContentLoading = false;
                $scope.loading = false;
            }, function (error) {
                showErrorToast("Ha ocurrido un error en la carga, por favor inténtalo de nuevo");
                $scope.loading = false;
                $scope.pagination.onContentLoading = false;
            });

            $scope.pagination.offset += $scope.pagination.itemsPerPage; // increment offset and limit for next ajax
            $scope.pagination.limit += $scope.pagination.itemsPerPage;
        }
    };

    //=====================================================================
    // Data watchers
    //=====================================================================

    $scope.$watch('data.cropFamily', function (newValue, oldValue) { // clean arrays and variables for new list
        if (newValue && newValue !== oldValue) {
            $scope.pagination.endOfPage = false;
            $scope.data.plague = "";
            $scope.data.plagues = [];
            $scope.data.productsFito = [];

            VademecumManager.getAllPlagues(newValue).then(function (response) {
                $scope.data.plagues = response.plain();
            }, function () {
                showErrorToast("Lo sentimos, ha ocurrido un error al cargar las plagas.");
            });
        }
    });

    $scope.$watch('data.plague', function (newValue, oldValue) {
        if (newValue !== oldValue && newValue) {
            $scope.data.productsFito = [];

            $scope.pagination.offset = 0;
            $scope.pagination.limit = $scope.pagination.itemsPerPage;
            $scope.nextPage(newValue);
        }
    });
}]);
