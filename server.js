var connect 	= require('connect');
var serveStatic = require('serve-static');

connect().use(serveStatic(__dirname)).listen(8080, function () {
	console.log("you can access server on http://localhost:8080");
});